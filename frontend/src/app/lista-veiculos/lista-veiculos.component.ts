import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import Swal from 'sweetalert2';
import { VeiculoModel } from '../models/veiculo.model';
import { VeiculoService } from '../services/veiculo.service';

@Component({
  selector: 'app-lista-veiculos',
  templateUrl: './lista-veiculos.component.html',
  styleUrls: ['./lista-veiculos.component.css'],
})
export class ListaVeiculosComponent implements OnInit {
  dataSource = new MatTableDataSource<VeiculoModel>();

  carregando = true;

  displayedColumns = [
    'id',
    'placa',
    'modelo',
    'anoModelo',
    'anoFabricacao',
    'marca',
    'fipe',
    'actions',
  ];

  filtroPlacaFipe = '';

  constructor(private veiculoService: VeiculoService) {}

  ngOnInit(): void {
    this.carregando = true;
    this.dataSource = new MatTableDataSource<VeiculoModel>();

    this.veiculoService.getAll().subscribe((response) => {
      this.dataSource = new MatTableDataSource<VeiculoModel>(
        response.map((item) => new VeiculoModel().deserialize(item))
      );

      this.carregando = false;
    });
  }

  procurar(): void {
    if (this.filtroPlacaFipe.trim() !== '') {
      this.carregando = true;
      this.dataSource = new MatTableDataSource<VeiculoModel>();

      this.veiculoService
        .getPlacaFipe(this.filtroPlacaFipe)
        .subscribe((response) => {
          this.dataSource = new MatTableDataSource<VeiculoModel>(
            response.map((item) => new VeiculoModel().deserialize(item))
          );

          this.carregando = false;
        });
    } else {
      this.ngOnInit();
    }
  }

  deletar(id: number): void {
    Swal.fire({
      title: 'Excluir veiculo',
      text: 'Confirmar exclusão do veiculo?',
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Não',
      confirmButtonText: 'Sim',
      showLoaderOnConfirm: true,
      allowOutsideClick: () => !Swal.isLoading(),
      preConfirm: () => {
        return this.veiculoService.delete(id);
      },
    }).then((result) => {
      if (result.value) {
        const response: any = result.value;

        if (response.success) {
          Swal.fire('', 'Veiculo excluido com sucesso!', 'success').then(() => {
            this.ngOnInit();
          });
        } else {
          Swal.fire('Erro', response.message, 'error');
        }
      }
    });
  }
}
