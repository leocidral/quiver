import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable()
export class VeiculoService {
  constructor(private http: HttpClient) {}

  protected getHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
    });
  }

  getAll(): Observable<any> {
    const options: any = {
      headers: this.getHeaders(),
      observe: 'response',
    };

    return this.http.get(environment.serverUrl + 'veiculos', options).pipe(
      map((res: any) => {
        return res.body;
      })
    );
  }

  getPlacaFipe(placaFipe: string): Observable<any> {
    const options: any = {
      headers: this.getHeaders(),
      observe: 'response',
    };

    return this.http
      .get(environment.serverUrl + 'veiculos/' + placaFipe, options)
      .pipe(
        map((res: any) => {
          return res.body;
        })
      );
  }

  getById(id: string): Observable<any> {
    const options: any = {
      headers: this.getHeaders(),
      observe: 'response',
    };

    return this.http
      .get(environment.serverUrl + 'veiculos/esp/' + id, options)
      .pipe(
        map((res: any) => {
          return res.body;
        })
      );
  }

  delete(id: number): Observable<any> {
    const options: any = {
      headers: this.getHeaders(),
      observe: 'response',
    };

    return this.http
      .delete(environment.serverUrl + 'veiculos/' + id, options)
      .pipe(
        map((res: any) => {
          return res.body;
        })
      );
  }

  add(dados: any): Observable<any> {
    const body = JSON.stringify(dados);

    const options: any = {
      headers: this.getHeaders(),
      observe: 'response',
    };

    return this.http
      .post(environment.serverUrl + 'veiculos', body, options)
      .pipe(
        map((res: any) => {
          return res.body;
        })
      );
  }
}
