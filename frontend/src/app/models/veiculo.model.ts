export class VeiculoModel {
  id: number;
  placa: string;
  modelo: string;
  anoModelo: number;
  anoFabricacao: number;
  marca: string;
  fipe: string;

  deserialize(input: any): this {
    Object.assign(this, input);
    return this;
  }
}
