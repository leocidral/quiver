import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { VeiculoService } from '../services/veiculo.service';

@Component({
  selector: 'app-cadastra-veiculos',
  templateUrl: './cadastra-veiculos.component.html',
  styleUrls: ['./cadastra-veiculos.component.css'],
})
export class CadastraVeiculosComponent implements OnInit {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private veiculoService: VeiculoService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.form = this.fb.group({
      placa: ['', Validators.compose([Validators.required])],
      modelo: ['', Validators.compose([Validators.required])],
      anoModelo: ['', Validators.compose([Validators.required])],
      anoFabricacao: ['', Validators.compose([Validators.required])],
      marca: ['', Validators.compose([Validators.required])],
      fipe: ['', Validators.compose([Validators.required])],
    });
  }

  ngOnInit(): void {}

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.form.controls[controlName];
    if (!control) {
      return false;
    }

    return (
      control.hasError(validationType) && (control.dirty || control.touched)
    );
  }

  salvar(): void {
    const controls = this.form.controls;

    if (this.form.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    Swal.fire({
      title: 'Cadastrar veiculo',
      text: 'Confirmar cadastro do veiculo?',
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Não',
      confirmButtonText: 'Sim',
      showLoaderOnConfirm: true,
      allowOutsideClick: () => !Swal.isLoading(),
      preConfirm: () => {
        return this.veiculoService.add(this.form.value);
      },
    }).then((result) => {
      if (result.value) {
        const response: any = result.value;

        if (response.success) {
          Swal.fire('', 'Veiculo cadastrado com sucesso!', 'success').then(
            () => {
              this.router.navigate(['../'], {
                relativeTo: this.activatedRoute,
              });
            }
          );
        } else {
          Swal.fire('Erro', response.message, 'error');
        }
      }
    });
  }
}
