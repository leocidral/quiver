import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { VeiculoService } from '../services/veiculo.service';

@Component({
  selector: 'app-visualiza-veiculos',
  templateUrl: './visualiza-veiculos.component.html',
  styleUrls: ['./visualiza-veiculos.component.css'],
})
export class VisualizaVeiculosComponent implements OnInit, OnDestroy {
  form: FormGroup;

  carregando = true;

  private subscriptions: Subscription[] = [];

  constructor(
    private fb: FormBuilder,
    private veiculoService: VeiculoService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.form = this.fb.group({
      id: [{ value: '', disabled: true }],
      placa: [{ value: '', disabled: true }],
      modelo: [{ value: '', disabled: true }],
      anoModelo: [{ value: '', disabled: true }],
      anoFabricacao: [{ value: '', disabled: true }],
      marca: [{ value: '', disabled: true }],
      fipe: [{ value: '', disabled: true }],
    });

    const routeSubscription = this.activatedRoute.params.subscribe((params) => {
      const id = params.id;

      this.veiculoService.getById(id).subscribe((response) => {
        if (response.success) {
          this.form.controls['id'].setValue(response.dados.id);
          this.form.controls['placa'].setValue(response.dados.placa);
          this.form.controls['modelo'].setValue(response.dados.modelo);
          this.form.controls['anoModelo'].setValue(response.dados.anoModelo);
          this.form.controls['anoFabricacao'].setValue(
            response.dados.anoFabricacao
          );
          this.form.controls['marca'].setValue(response.dados.marca);
          this.form.controls['fipe'].setValue(response.dados.fipe);
        } else {
          this.router.navigate(['../../'], {
            relativeTo: this.activatedRoute,
          });
        }

        this.carregando = false;
      });
    });

    this.subscriptions.push(routeSubscription);
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.subscriptions.forEach((sb) => sb.unsubscribe());
  }
}
