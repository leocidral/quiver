import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizaVeiculosComponent } from './visualiza-veiculos.component';

describe('VisualizaVeiculosComponent', () => {
  let component: VisualizaVeiculosComponent;
  let fixture: ComponentFixture<VisualizaVeiculosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizaVeiculosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizaVeiculosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
