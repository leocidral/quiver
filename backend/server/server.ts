import * as restify from "restify";
import * as corsMiddleware from "restify-cors-middleware";
import { environment } from "../configs/environment";
import { Router } from "../configs/router";

export class Server {
  application: restify.Server;

  initRoutes(routers: Router[]): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        this.application = restify.createServer({
          name: "quiver-api",
          version: "1.0.0",
        });

        const corsOptions: corsMiddleware.Options = {
          preflightMaxAge: 10,
          origins: ["*"],
          allowHeaders: [""],
          exposeHeaders: [""],
        };

        const cors: corsMiddleware.CorsMiddleware = corsMiddleware(corsOptions);

        this.application.pre(cors.preflight);

        this.application.use(cors.actual);
        this.application.use(restify.plugins.queryParser());
        this.application.use(restify.plugins.bodyParser());

        for (let router of routers) {
          router.applyRoutes(this.application);
        }

        this.application.listen(environment.server.port, () => {
          resolve(this.application);
        });
      } catch (error) {
        reject(error);
      }
    });
  }

  bootstrap(routers: Router[] = []): Promise<Server> {
    return this.initRoutes(routers).then(() => this);
  }
}
