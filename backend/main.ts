import { Server } from "./server/server";
import { veiculosRouter } from "./veiculos/veiculos.router";

const server = new Server();

server
  .bootstrap([veiculosRouter])
  .then((server) => {
    console.log("running bootstrap");
  })
  .catch((error) => {
    console.log("server failed");
    console.log(error);
    process.exit(1);
  });
