import { Router } from "../configs/router";
import * as restify from "restify";
import { Veiculo, Veiculos } from "./veiculos.model";

class VeiculosRouter extends Router {
  applyRoutes(application: restify.Server) {
    application.get("/veiculos", (req, resp, next) => {
      Veiculos.findAll().then((veiculos) => {
        resp.json(veiculos);
        return next();
      });
    });

    application.get("/veiculos/:placaFipe", (req, resp, next) => {
      Veiculos.findByPlacaFipe(req.params.placaFipe).then((veiculos) => {
        resp.json(veiculos);
        return next();
      });
    });

    application.get("/veiculos/esp/:id", (req, resp, next) => {
      Veiculos.findById(req.params.id).then((veiculos) => {
        resp.json(veiculos);
        return next();
      });
    });

    application.post("/veiculos", (req, resp, next) => {
      const newVeiculo = new Veiculo(req.body);

      Veiculos.add(newVeiculo).then(() => {
        resp.json({ success: true });
        return next();
      });
    });

    application.del("/veiculos/:id", (req, resp, next) => {
      Veiculos.delete(req.params.id).then((retorno) => {
        resp.json(retorno);
        return next();
      });
    });
  }
}

export const veiculosRouter = new VeiculosRouter();
