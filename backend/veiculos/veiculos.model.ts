export class Veiculo {
  id: number;
  placa: string;
  modelo: string;
  anoModelo: number;
  anoFabricacao: number;
  marca: string;
  fipe: string;

  constructor(body: any) {
    this.id = body.id;
    this.placa = body.placa;
    this.modelo = body.modelo;
    this.anoModelo = body.anoModelo;
    this.anoFabricacao = body.anoFabricacao;
    this.marca = body.marca;
    this.fipe = body.fipe;
  }
}

const veiculos = [
  new Veiculo({
    id: 1,
    placa: "ABC0001",
    modelo: "CELTA",
    anoModelo: 2010,
    anoFabricacao: 2009,
    marca: "CHEVROLET",
    fipe: "AAA",
  }),
  new Veiculo({
    id: 2,
    placa: "ABC0002",
    modelo: "UNO VIVACE",
    anoModelo: 2011,
    anoFabricacao: 2011,
    marca: "FIAT",
    fipe: "BBB",
  }),
  new Veiculo({
    id: 3,
    placa: "ABC0003",
    modelo: "VERACRUZ",
    anoModelo: 2011,
    anoFabricacao: 2010,
    marca: "HYUNDAI",
    fipe: "CCC",
  }),
  new Veiculo({
    id: 4,
    placa: "ABC0004",
    modelo: "GOL",
    anoModelo: 2015,
    anoFabricacao: 2014,
    marca: "VOLKSWAGEN",
    fipe: "DDD",
  }),
  new Veiculo({
    id: 5,
    placa: "ABC0005",
    modelo: "TUCSON",
    anoModelo: 2017,
    anoFabricacao: 2016,
    marca: "HYUNDAI",
    fipe: "EEE",
  }),
  new Veiculo({
    id: 6,
    placa: "ABC0006",
    modelo: "A3",
    anoModelo: 2014,
    anoFabricacao: 2013,
    marca: "AUDI",
    fipe: "FFF",
  }),
  new Veiculo({
    id: 7,
    placa: "ABC0007",
    modelo: "A4",
    anoModelo: 2015,
    anoFabricacao: 2014,
    marca: "AUDI",
    fipe: "GGG",
  }),
  new Veiculo({
    id: 8,
    placa: "ABC0008",
    modelo: "BLAZER",
    anoModelo: 2009,
    anoFabricacao: 2008,
    marca: "CHEVROLET",
    fipe: "HHH",
  }),
  new Veiculo({
    id: 9,
    placa: "ABC0009",
    modelo: "CAPTIVA",
    anoModelo: 2019,
    anoFabricacao: 2018,
    marca: "CHEVROLET",
    fipe: "III",
  }),
  new Veiculo({
    id: 10,
    placa: "ABC0010",
    modelo: "COBALT",
    anoModelo: 2020,
    anoFabricacao: 2019,
    marca: "CHEVROLET",
    fipe: "JJJ",
  }),
];

export class Veiculos {
  static findAll(): Promise<Veiculo[]> {
    return Promise.resolve(veiculos);
  }

  static findByPlacaFipe(placaFipe: string): Promise<Veiculo[]> {
    return new Promise((resolve) => {
      const filtered = veiculos.filter(
        (item) => item.placa == placaFipe || item.fipe == placaFipe
      );

      resolve(filtered);
    });
  }

  static findById(Id: number): Promise<any> {
    return new Promise((resolve) => {
      const filtered = veiculos.filter((item) => item.id == Id);

      if (filtered.length > 0) {
        resolve({ success: true, dados: filtered[0] });
      }

      resolve({ success: false, message: "Veiculo não encontrado" });
    });
  }

  static add(veiculo: Veiculo): Promise<void> {
    return new Promise((resolve) => {
      veiculo.id = veiculos.length + 1;
      veiculos.push(veiculo);

      resolve();
    });
  }

  static delete(id: number): Promise<{ success: boolean; message: string }> {
    return new Promise((resolve) => {
      const index = veiculos.findIndex((item) => item.id == id);

      if (index != -1) {
        veiculos.splice(index, 1);
        resolve({ success: true, message: "" });
      }

      resolve({ success: false, message: "Veiculo não encontrado" });
    });
  }
}
